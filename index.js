// General

const deepSet = (root, path, value) => (
  path.reduce((obj, key, idx) => obj[key] = obj[key] || (idx == path.length - 1 ? [] : {}), root).push(value), root
)

const toNumIfPossible = (value) =>
  isNaN(value) ? value : +value

// Group

const createObjectsWithGroupKey = (objects, extractors) =>
  objects.map(object => ({ keys: extractors.map(e => e(object)), object }))

const createGroupObject = (objectsWithKey) => 
  objectsWithKey.reduce((root, object) => deepSet(root, object.keys, object.object), {})

const objToArray = (groupObj) => 
  Object.keys(groupObj).reduce((arr, key) => [...arr, [toNumIfPossible(key), groupObjectToArray(groupObj[key])]], [])

const groupObjectToArray = (groupObj) => 
  Array.isArray(groupObj) ? groupObj : objToArray(groupObj)

const group = (arrayOfObjects, extractors) => {
  if (extractors.length === 0)
    return arrayOfObjects
  const objectsWithKeys = createObjectsWithGroupKey(arrayOfObjects, extractors)
  const groupObject = createGroupObject(objectsWithKeys)
  return groupObjectToArray(groupObject)
}

// Where

const where = (data, filters) => 
  data.filter((value) => filters.reduce((acc, fil) => acc || fil(value), false))
  
// From

const join = (table1, table2, cond) => 
  table1.reduce((a, b) => (a.push(...table2.map(c => [b, c])), a), []).filter(j => cond(j))

const makeTempTable = (tables, conditions) => {
  const [table1, table2] = tables
  return tables.length === 2
    ? join(table1, table2, conditions ? conditions.shift() : (() => true))
    : table1 || []
}

// Select

const select = (array, selectCallback) => 
  array.map(selectCallback)

const query = function() {
  /**
   * Query builder
   */
  const builder = {
    select: function(selectCallback) {
      if (this.selectCallback !== undefined)
        throw new Error("Duplicate SELECT")
        
      this.selectCallback = selectCallback || null
      return this
    },
    from: function() {
      if (this.data !== undefined)
        throw new Error("Duplicate FROM")
       
      this.data = Object.values(arguments)
      return this
    },
    where: function() {
      this.whereCallbacks = [...(this.whereCallbacks || []), ...arguments]
      return this
    },
    groupBy: function() {
      if (this.groupByCallbacks !== undefined)
        throw new Error("Duplicate GROUPBY")
        
      this.groupByCallbacks = Object.values(arguments)
      return this
    },
    orderBy: function(orderByCallback) {
      if (this.orderByCallback !== undefined)
        throw new Error("Duplicate ORDERBY")
        
      this.orderByCallback = orderByCallback || null
      return this
    },
    having: function(havingCallback) {
      this.havingCallback = havingCallback || null
      return this
    },
    execute: function() {
      const tables = builder.data || []
      const tempTable = makeTempTable(tables, this.whereCallbacks)
      
      const whereCallbacks = (this.whereCallbacks || []).length ? this.whereCallbacks : [() => true]
      const filtered = where(tempTable, whereCallbacks)
      
      const groupByCallbacks = builder.groupByCallbacks || []
      const grouped = group(filtered, groupByCallbacks)
      
      const orderByCallback = builder.orderByCallback || (() => 0)
      const ordered = grouped.sort(orderByCallback)

      const havingCallback = builder.havingCallback || ((value) => value)
      const haved = ordered.filter(havingCallback)
      
      const selectCallback = builder.selectCallback || ((value) => value)
      const selected = select(haved, selectCallback)
      return selected
    }
  }
  return builder
}